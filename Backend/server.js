const config = require('./config/config');
const express = require('express');
const cors = require('cors');
const { updateData, wateringCheck, wateringUpdate, waterRefill} = require('./src/endpoints/endpoints');
const port = config.port;

const server = express();

server.use(cors());
server.use(express.json());

server.listen(port);

server.post('/add-data', updateData);
server.get("/watering", wateringCheck);
server.post("/watering", wateringUpdate);
server.get("/water-refill", waterRefill);

