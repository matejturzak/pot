const database = require('../connection/Database.js');
const log = require('../log');

class DatabaseOperations {
    async getLastRemainingWater() {
        try {
            const connection = await database.provideConnection();
            const response = await connection.query("select remainingWater from sensors order by id desc limit 1;");
            return response[0].remainingWater;
        } catch (err) {
            log.error(`An error occurred while obtaining remaining water from DB : ` + err);
            throw err;
        } finally {
            await database.releaseConnection()
        }
    }

    async getSoilMoistureData() {
        try {
            const connection = await database.provideConnection();
            return await connection.query("select soilMoisture from sensors order by id desc limit 8;");
        } catch (err) {
            log.error(`An error occurred while obtaining soil moisture data from DB : ` + err);
            throw err;
        } finally {
            await database.releaseConnection()
        }
    }

    async updateRemainingWater(newWaterValue) {
        try {
            const connection = await database.provideConnection();
            return await connection.query("UPDATE sensors SET remainingWater = '" + newWaterValue + "' order by ID desc limit 1");
        } catch (err) {
            log.error(`An error occurred while obtaining last water status from DB : ` + err);
            throw err;
        } finally {
            await database.releaseConnection()
        }
    }

    async addReadings(fetchedData) {
        const {humidity_value, temperature_value, soil_value} = fetchedData;
        const lastRemainingWaterStatus = await this.getLastRemainingWater();
        try {
            const connection = await database.provideConnection();
            await connection.query("INSERT INTO sensors (temperature, humidity, soilMoisture, remainingWater, watered) VALUES ('" + temperature_value + "', '" + humidity_value + "', '" + soil_value + "', '" + lastRemainingWaterStatus + "', 0)");
        } catch (err) {
            log.error(`An error occurred while obtaining remaining water from DB : ` + err);
            throw err;
        } finally {
            await database.releaseConnection()
        }
    }

    async updateWateredStatus() {
        try {
            const connection = await database.provideConnection();
            await connection.query("UPDATE sensors SET watered = True order by ID desc limit 1");
        } catch (err) {
            log.error(`An error occurred while obtaining remaining water from DB : ` + err);
            throw err;
        } finally {
            await database.releaseConnection()
        }
    }

}

module.exports = new DatabaseOperations();

