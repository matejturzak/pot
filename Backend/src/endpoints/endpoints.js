const log = require('../log');
const { wateringCalculation } = require('../logic');
const operation = require('../databaseOperations/DatabaseOperations');


module.exports = {
    waterRefill: async (req, res) => {
        const amountOfAddedWater = req.body.value;
        try {
            const remainingWater = await operation.getLastRemainingWater();
            const newWaterValue = remainingWater + parseInt(amountOfAddedWater);
            await operation.updateRemainingWater(newWaterValue);
            res.status(200).send()
        } catch (err) {
            log.error(`Could refill water tank due to: ` + err);
            res.status(500).send()
        }
    },

    wateringCheck: async (req, res) => {
        try {
            const soilMoistureData = await operation.getSoilMoistureData();
            const remainingWater = await operation.getLastRemainingWater();
            const wateringRequired = wateringCalculation(soilMoistureData, remainingWater);
            if (wateringRequired) {
                res.status(200).send({toBeWatered: true})
            } else {
                res.status(200).send({toBeWatered: false})
            }
        } catch (err) {
            log.error(`Could not verify if watering is necessary due to: ` + err);
            res.status(500).send()
        }
    },

    wateringUpdate: async (req, res) => {
        try {
            const watered = req.body.watered;
            const soilMoistureData = await operation.getSoilMoistureData();
            const remainingWater = await operation.getLastRemainingWater();
            const wateringRequired = wateringCalculation(soilMoistureData, remainingWater);
            if (wateringRequired && watered) {
                await operation.updateRemainingWater(remainingWater -75);
                await operation.updateWateredStatus();
                res.status(200).send()
            } else {
                res.status(406).send()
            }
        } catch (err) {
            log.error(`Could not verify if watering is necessary due to: ` + err);
            res.status(500).send()
        }
    },

    updateData: async (req, res) => {
        const fetchedData = req.body;
        try {
            await operation.addReadings(fetchedData);
            res.status(200).send()
        } catch (err) {
            log.error(`Could not upload new data due to: ` + err);
            res.status(500).send()
        }
    }

};