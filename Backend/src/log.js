let winston = require('winston');
let config = require('../config/config');

module.exports = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.timestamp({format:'HH:mm:ss DD.MM.YYYY'}),
        winston.format.json()
    ),
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            level: 'info'
        }),
        new winston.transports.File({filename: config.errorLogFile, level: 'error',}),
        new winston.transports.File({filename: config.logFile})
    ]
});
