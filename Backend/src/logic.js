const email = require('./connection/EmailClient');


module.exports = {
    wateringCalculation: (soilMoistureData, remainingWater) => {
        const sameMoistureValue = soilMoistureData.filter(item => item.soilMoisture === soilMoistureData[0].soilMoisture);
        const averageSoilMoisture = soilMoistureData.reduce((sum, {soilMoisture}) => sum + soilMoisture, 0) / soilMoistureData.length;
        const lastSoilMoistureValue = parseInt(soilMoistureData[0].soilMoisture);
        if (parseInt(remainingWater) > 150 && sameMoistureValue.length < 15) {
            if (parseInt(averageSoilMoisture) < 40 && parseInt(lastSoilMoistureValue) < 65) {
                return true
            }
        }
        if (parseInt(remainingWater) < 150) {
            module.exports.sendEmail()
        }
        return false
    },

    sendEmail: async () => {
        await email.sendEmail()
    }

};