const mariadb = require('mariadb');
const log = require('../log');
const config = require('../../config/config');

class Database {
    constructor() {
        this.pool =  mariadb.createPool({
            host: config.mariadb.host,
            port: config.mariadb.port,
            user: config.mariadb.user,
            password: config.mariadb.password,
            connectionLimit: config.mariadb.connectionLimit,
            database: config.mariadb.database
        });
        this.connection = null;
    }

    async provideConnection() {
        try {
            this.connection = await this.pool.getConnection();
            return this.connection
        } catch (err) {
            log.error(`An error occurred while connecting to DB : ` + err);
            throw new Error(err);
        }
    }

    async releaseConnection() {
        try {
            this.connection.release();
        } catch (err) {
            log.error(`An error occurred while releasing connection to DB : ` + err);
            throw new Error(err);
        }
    }

}

module.exports =  new Database();
