const nodemailer = require("nodemailer");
const config = require('../../config/config');
const log = require('../log');

class EmailClient {
    #transporter;

     constructor() {
        this.#transporter = this.createTransporter()
    }

    createTransporter() {
        try {
            return nodemailer.createTransport(config.service)
        } catch (err) {
            log.error(`An error occurred while sending email : ` + err);
            throw new Error(err);
        }

    }

    async sendEmail() {
        try {
            await this.#transporter.sendMail(config.mail)
        } catch (err) {
            log.error(`An error occurred while sending email : ` + err);
            throw new Error(err);
        }
    }
}


module.exports = new EmailClient();
