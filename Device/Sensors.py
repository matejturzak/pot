import spidev
import Adafruit_DHT
from numpy import interp
from datetime import datetime as datetime

class Sensors:

    def __init__(self):
        spi = spidev.SpiDev()
        spi.open(0, 0)
        spi.max_speed_hz = 1350000
        self.spi_session = spi

    def analog_input(self, channel):
        reading = self.spi_session.xfer2([1, (8 + channel) << 4, 0])
        sensor_data = ((reading[1] & 3) << 8) + reading[2]
        return sensor_data

    def process_analaog_data(self, soil_analog1, analog_data2):
        humidity, temperature = Adafruit_DHT.read_retry(22, 16)
        adjusted_humidity = round(float(humidity), 5)
        adjusted_temperature = round(temperature, 5)
        soil_value1 = interp(soil_analog1, [380, 700], [100, 0])
        soil_value2 = interp(analog_data2, [380, 700], [100, 0])
        adjusted_soil_humidity = float((soil_value1 + soil_value2) / 2)
        time_of_measurement = datetime.now().strftime("%H:%M:%S %d/%m/%Y")
        return adjusted_humidity, adjusted_temperature, adjusted_soil_humidity, time_of_measurement
