from Sensors import Sensors
from Display import DisplayData
from Requests import Requests
from Relay import Relay
import RPi.GPIO as GPIO
from time import sleep

class Slave:

    def __init__(self):
        self.sensors = Sensors()
        self.display = DisplayData()
        self.requests = Requests()
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.relay = Relay()
        self.i = 1

    def work(self):
        while True:
            analog_data1 = self.sensors.analog_input(0)
            analog_data2 = self.sensors.analog_input(1)
            humidity, temperature, moisture, time_of_measurement = self.sensors.process_analaog_data(analog_data1, analog_data2)
            backlighting = self.display.display_backlight_adjuster()
            self.requests.upload_data(humidity, temperature, moisture)
            
            while self.i != 20:
                if backlighting == True:
                    self.display.display_output(humidity, temperature, moisture, time_of_measurement) 
                sleep(180)
                self.i += 1
            
            should_be_watered = self.requests.verify_watering()

            if should_be_watered:
                self.relay.water()
                self.requests.update_successful_watering(should_be_watered)
                
            self.i = 1

    

 

job = Slave()
job.work()