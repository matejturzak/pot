import requests

class Requests:
    def __init__(self):
        self.url = 'http://192.168.0.13:3000'

    def upload_data(self, hum, temp, moist):
        payload = {'humidity_value': hum, 'temperature_value': temp, 'soil_value': moist}
        requests.post(self.url + '/add-data', json=payload)

    def verify_watering(self):
        response = requests.get(self.url + '/watering')
        if response.status_code == 200:
            return response.json()['toBeWatered']
        else:
            return False

    def update_successful_watering(self, watered):
        requests.post(self.url + '/watering', json={'watered': watered})
