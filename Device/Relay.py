import RPi.GPIO as GPIO
from time import sleep


class Relay:
    def __init__(self):
        GPIO.setup(20, GPIO.OUT)
        GPIO.output(20, GPIO.HIGH)

    def water(self):
        GPIO.output(20, GPIO.LOW)
        sleep(4)
        GPIO.output(20, GPIO.HIGH)
