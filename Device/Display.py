import driver
import time


class DisplayData:
    def __init__(self):
        self.display = driver.Lcd()
        self.backlighting = True
        self.display.lcd_clear()
        self.num_cols = 20
        self.num_line = 1


    def display_backlight_adjuster(self):
        current_hour = time.localtime(time.time()).tm_hour
        if ((0 <= current_hour < 8) and self.backlighting == True):
            self.backlighting = False
            self.display.lcd_backlight(0)
        elif (current_hour >= 8 and self.backlighting == False):
            self.backlighting = True
            self.display.lcd_backlight(1)     
        return self.backlighting
        

    def first_line_of_display(self, time_of_measurement):
        text = "Last measurement done " + time_of_measurement
        if (len(text) > self.num_cols):
            self.display.lcd_display_string(text[:self.num_cols], self.num_line)
            time.sleep(1)
            for i in range(len(text) - self.num_cols + 1):
                text_to_print = text[i:i + self.num_cols]
                self.display.lcd_display_string(text_to_print, self.num_line)
            time.sleep(0.2)
        else:
            self.display.lcd_display_string(text, self.num_line)

    def display_output(self, hum, temp, moist, time_of_measurement):
        self.display.lcd_display_string("Air humidity: " + str(round(hum, 1)), 2)
        self.display.lcd_display_string("Temperature: " + str(round(temp, 1)), 3)
        self.display.lcd_display_string("Soil moisture: " + str(round(moist, 1)), 4)
        self.first_line_of_display(time_of_measurement)
